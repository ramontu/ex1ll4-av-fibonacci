
import acm.program.ConsoleProgram;

public class fibonacci extends ConsoleProgram {
    int f0 = 0;
    int f1 = 1; //f-1
    int f2 = 0; //f-2
    int actual = 0;
    public void run(){
        int ite = readInt("Introdueix el valor en que vols calcular el resultat: ");
        int resultat = fibonacci(ite);
        println("El resultat es: "+resultat);
    }


    public int fibonacci(int iteracions){
        f2 = f1;
        f1 = f0;
        f0 = f1+f2;
        actual++;
        while (actual< iteracions){
            fibonacci(iteracions);

        }
        return f0;
    }
}
